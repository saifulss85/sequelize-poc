'use strict';

module.exports = (sequelize, DataTypes) => {
  const ShortLink = sequelize.define('ShortLink', {
    short_url: DataTypes.STRING,
    long_url: DataTypes.STRING,
  }, {
    tableName: 'short_links',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    sequelize
  });

  ShortLink.associate = function(models) {
    // associations can be defined here
  };

  return ShortLink;
};
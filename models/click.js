'use strict';
module.exports = (sequelize, DataTypes) => {
  const Click = sequelize.define('Click', {
    client_remote_ip: DataTypes.STRING,
  }, {
    tableName: 'clicks',
    createdAt: 'created_at',
    updatedAt: false,
    sequelize
  });

  Click.associate = function (models) {
    // associations can be defined here
  };

  return Click;
};
const models = require('../models');

const truncateTable = async modelName => {
  await models[modelName].destroy({
    where: {},
    force: true,
  });
};

module.exports = function truncate(modelNameAsString) {
  if (modelNameAsString) {
    return truncateTable(modelNameAsString);
  }

  return Promise.all(
    Object.keys(models).map((key) => {
      if (['sequelize', 'Sequelize'].includes(key)) return null;
      return truncateTable(key);
    })
  );
};
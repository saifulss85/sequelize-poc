const models = require('../../models');
const { Click } = models;
const truncate = require('../truncate');

describe('Click model', () => {
  let click;

  beforeEach(async () => {
    await truncate();

    click = await Click.create({
      client_remote_ip: '111.111.111.111',
    });
  });

  afterAll(async () => {
    await models.sequelize.connectionManager.close();
  });

  it('should create a new click', async () => {
    const fetchedClick = await Click.findByPk(click.id);
    expect(fetchedClick).toBeDefined();
  });
});
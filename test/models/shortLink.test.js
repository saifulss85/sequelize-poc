const models = require('../../models');
const { ShortLink } = models;
const truncate = require('../truncate');

describe('ShortLink model', () => {
  let shortLink;

  beforeEach(async () => {
    await truncate();

    shortLink = await ShortLink.create({
      short_url: '87Ds8',
      long_url: 'http://example.com',
    });
  });

  afterAll(async () => {
    await models.sequelize.connectionManager.close();
  });

  it('should create a new shortLink', async () => {
    const fetchedShortLink = await ShortLink.findByPk(shortLink.id);
    expect(fetchedShortLink).toBeDefined();
  });

  it('should soft-delete record when destroy is called', async () => {
    // given a shortLink in database

    // when we delete the shortLink
    await shortLink.destroy();

    // then the shortLink should be soft-deleted
    expect(shortLink.isSoftDeleted()).toBeTruthy();
  });
});
const models = require('../../models');
const { User } = models;
const truncate = require('../truncate');

describe('User model', () => {
  let user;

  beforeEach(async () => {
    await truncate();

    user = await User.create({
      first_name: 'Rebecca',
      last_name: 'Romijn-Stamos',
      email: 'rrs@example.com',
    });
  });

  afterAll(async () => {
    await models.sequelize.connectionManager.close();
  });

  it('should create a new user', async () => {
    const fetchedUser = await User.findByPk(user.id);
    expect(fetchedUser).toBeDefined();
  });

  it('should soft-delete record when destroy is called', async () => {
    // given a user in database

    // when we delete the user
    await user.destroy();

    // then the user should be soft-deleted
    expect(user.isSoftDeleted()).toBeTruthy();
  });
});
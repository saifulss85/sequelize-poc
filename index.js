const truncate = require('./test/truncate');

// testRelationships();

async function testRelationships() {
  await truncate();
  const user = createNewUser();
  const shortLink = createNewShortLink();
}

function truncateUsersTable() {
  truncate('User');
}

async function createNewUser() {
  const { User } = require('./models');
  return await User.create({
    first_name: 'Rebecca',
    last_name: 'Romijn-Stamos',
    email: 'rrs@example.com',
  });
}

async function createNewShortLink() {
  const { ShortLink } = require('./models');
  return await ShortLink.create({
    short_url: 'HGd7s6',
    long_url: 'http://example.com',
  });
}

function createNewClick() {
  const { Click } = require('./models');
  Click.create({
    client_remote_ip: '123.123.123.123',
  });
}